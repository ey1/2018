---
abstract: The most commonly used technique for diagnosing malaria is microscopy of
  stained blood smears. However, even skilled microscopists can miss infections with
  low parasite numbers. I am using Keras to develop a convolutional neural net to
  enable parasite detection, even at low numbers in the blood.
level: Intermediate
speakers:
- Tobias "Tobi" Schraink
title: Diagnosing malaria with Keras and microscopy images
type: talk
---

Classifying microscopy images is different from, for example, wild-life photography and traffic images typically seen in competitions like ImageNet, since there are no easily identifiable borders between objects in the microscopy images. I am approaching this problem as an image classification and an image segmentation task, and I will describe the obstacles encountered while building a reliable model for identification of malaria parasites. My talk will include a broad description of convolutional neural networks, how they can be used for detecting malaria parasites in microscopy images, and what unique challenges come with using deep learning with microscopy images.