---
abstract: 'A Burn out story, w/ the OpenStack project, 25K+ contributors international,

  300,000 core deployment for CERN labs Geneva, & powers 50+ of the fortune 100 companies.
  An 8-year journey story will be expressed by words, music w/ a midi keyboard, python
  & a human attempting to hold his composure.'
level: All
speakers:
- Mike Perez
title: 'Talking About Burnout: An Attempt of Coping with Python and Music'
type: talk
---

Writing up a burnout talk to add to the community makes me sad, but we have to talk to begin healing ourselves to be strong for others in listening.

The OSS project OpenStack, a community of 25,000+ people contributing international together, powers large international scale deployments like the 300,000+ cores for CERN labs in Geneva, and more than half of the fortune 100 companies in the US.

The accomplishments & failures of the project would create myriad mixed opinions. Being an advocate at this scale for the OpenStack Foundation was very tough.

Here's a story of an 8-year journey with the community that will be expressed by words, music powered by a midi keyboard, python and a human who is going to attempt to hold his composure.

Disclaimer: Unfortunately the story includes pieces that could relate to trauma experiences similar to others that members of the audience may have experienced, but the speaker will be respectful and attempt careful usage of words to avoid those triggers.