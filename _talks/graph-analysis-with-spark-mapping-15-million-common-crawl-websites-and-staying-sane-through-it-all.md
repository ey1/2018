---
abstract: Use the Internet? Fond of graphs? Then this talk may be for you! Learn how
  to build and explore graphs with Spark GraphFrames. I will be sharing tips, tricks,
  and gotchas I learned for conducting network analysis with Spark, and diving into
  the exciting problems you can represent with graphs.
level: Intermediate
speakers:
- Win Suen
title: 'Graph Analysis with Spark: Mapping >15 million Common Crawl websites (and
  staying sane through it all)'
type: talk
---

As the web grows ever larger and more content-rich, graph analysis may be one of the most powerful tools for unlocking insights within the mythical big data. That's totally not fluff, because WIRED wrote about it (https://www.wired.com/insights/2014/03/graph-theory-key-understanding-big-data/). 

This talk relates to ongoing work to generate a web link graph using the Amazon Common Crawl, and to find insights into how different websites interact with each other (sometimes in surprising ways!). Spark GraphFrames was integral to exploring an enormous and wildly dirty dataset, and the data size really pushed the tool to its limits. Along the way, I learned a great deal about optimizations in representing and computing graphs.

We'll talk about:

* The types of problems graph analysis can solve.
* How Spark GraphFrames work under the hood.
* Computational optimizations that will help you in your journey (you're only as good as your data structure).
* Things I discovered when exploring the Common Crawl data.

And much more! Github repo with all code will also be shared.