---
name: Equinox
tier: gold
site_url: http://tech.equinox.com/work-with-us/
logo: equinox.png
---
We are a company with integrated luxury and lifestyle offerings centered on movement, nutrition and
regeneration. In addition to Equinox, our other brands, Blink, Pure Yoga, SoulCycle and Furthermore
are all recognized for inspiring and motivating members and employees to maximize life. Within our
portfolio of brands, we have more than 200 locations within every major city across the country in
addition to London, Toronto and Vancouver.
