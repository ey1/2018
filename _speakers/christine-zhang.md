---
name: Christine Zhang
talks:
- The Sum of Nothing
---

I'm a journalist and data analyst who loves stats, stories, spreadsheets, and sandwiches ... and alliteration! I did a Knight-Mozilla OpenNews Fellowship at the Los Angeles Times Data Desk, where I was R user on a team of (primarily) Pythonistas. Recently, I decided to challenge myself to get savvy with Python––or, in Harry Potter terms (I LOOOVE Harry Potter), learn some Parseltongue~~~

It's been a journey but I'm getting there, particularly with pandas, my love/hate Python package of choice.