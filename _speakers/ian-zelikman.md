---
name: Ian Zelikman
talks:
- The stories of the most infamous bugs
---

Ian has been been developing code for over 10 years.

He started his career developing application for weather forecasting and analysis.
Ian has been working with Python for over 6 years developing scalable and reliable back-end systems.
He find it most gratifying to develop scalable and maintainable code, and find Python to be one of the most effective tools at his disposal for that goal. 

In his recent role he spends most of his time developing complex and reliable pipelines for processing one of the largest healthcare datasets in the world.