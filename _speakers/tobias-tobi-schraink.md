---
name: Tobias "Tobi" Schraink
talks:
- Diagnosing malaria with Keras and microscopy images
---

I am a PhD student in Jane Carlton’s lab at the Center for Genomics and Systems Biology at NYU. Though my background is in molecular biology, the wonderful python ecosystem and its inhabitants have made it possible for me to answer computational questions myself. Currently I am working on developing molecular tools for malaria diagnosis.